from django.contrib import admin
from .models import *
from django.forms import ModelChoiceField, ModelForm, ValidationError
from django.utils.safestring import mark_safe


# Валидация на SD карту (не забыть включить form в в админ модель)
class SmartphoneAdminForm(ModelForm):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = kwargs.get('instance')
        self.fields['image'].help_text = mark_safe('<span style="color:red; font-size: 12px;">Загружайте изображение'
                                                   ' с минимальным разрешением {}x{}</span>'.format
                                                   (*Product.MIN_RESOLUTION))

        # Красим плашку в серый цвет, если нет SD
        if instance and not instance.sd:
            self.fields['sd_volume_max'].widget.attrs.update({
                'readonly': True, 'style': 'background: lightgrey;'
            })

    # Условие на отсутсвие SD карты
    def clean(self):
        if not self.cleaned_data['sd']:
            self.cleaned_data['sd_volume_max'] = None
        return self.cleaned_data

    # Проверка разрешения картинки (min, max, max_size)
    def clean_image(self):
        image = self.cleaned_data['image']
        img = Image.open(image)
        min_height, min_width = Product.MIN_RESOLUTION
        max_height, max_width = Product.MAX_RESOLUTION
        if image.size > Product.MAX_IMAGE_SIZE:
            raise ValidationError('Размер изображения не должен превышать 3 МВ!')
        if img.width < min_width or img.height < min_height:
            raise ValidationError('Разрешение изображения меньше минимального!')
        if img.width > max_width or img.height > max_height:
            raise ValidationError('Разрешение изображения больше максимального!')
        return image


class NotebookAdminForm(ModelForm):

    # Help текст
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['image'].help_text = mark_safe('<span style="color:red; font-size: 12px;">Загружайте изображение'
                                                   ' с минимальным разрешением {}x{}</span>'.format
                                                   (*Product.MIN_RESOLUTION))

    # Проверка разрешения картинки (min, max, max_size)
    def clean_image(self):
        image = self.cleaned_data['image']
        img = Image.open(image)
        min_height, min_width = Product.MIN_RESOLUTION
        max_height, max_width = Product.MAX_RESOLUTION
        if image.size > Product.MAX_IMAGE_SIZE:
            raise ValidationError('Размер изображения не должен превышать 3 МВ!')
        if img.width < min_width or img.height < min_height:
            raise ValidationError('Разрешение изображения меньше минимального!')
        if img.width > max_width or img.height > max_height:
            raise ValidationError('Разрешение изображения больше максимального!')
        return image


# Привязка продукта к конкертной категории
class NotebookAdmin(admin.ModelAdmin):

    form = NotebookAdminForm

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'category':
            return ModelChoiceField(Category.objects.filter(slug='notebooks'))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


# Привязка продукта к конкертной категории
class SmartphoneAdmin(admin.ModelAdmin):

    change_form_template = 'admin.html'
    form = SmartphoneAdminForm

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'category':
            return ModelChoiceField(Category.objects.filter(slug='smartphones'))
        return super().formfield_for_foreignkey(db_field, request, **kwargs)


admin.site.register(Category)
admin.site.register(Notebook, NotebookAdmin)
admin.site.register(Smartphone, SmartphoneAdmin)
admin.site.register(CartProduct)
admin.site.register(Cart)
admin.site.register(Customer)
admin.site.register(Order)
