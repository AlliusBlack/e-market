from django import forms

from .models import Order


# Форма заказа покупателя. P.S Устанавливаем django-crispy-forms для хорошего отображения формы
class OrderForm(forms.ModelForm):

    # Переопределяем название после редактирования формы
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['order_date'].label = "Дата получения заказа"

    # Редактируем поле формы дата (делаем календарь)
    order_date = forms.DateField(widget=forms.TextInput(attrs={'type': 'date'}))

    class Meta:
        model = Order
        fields = (
            'first_name', 'last_name', 'phone', 'address', 'buying_type', 'order_date', 'comment',
        )
