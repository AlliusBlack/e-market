from django import template
from django.utils.safestring import mark_safe
from mainapp.models import Smartphone

register = template.Library()

# Созданием собираемый шаблон

# Голова шаблона
TABLE_HEAD = """
                <table class="table" style="margin-left: 20px; width:90%">
                  <tbody>
            """

# Ноги шаблона
TABLE_TAIL = """
                 </tbody>
                </table>
            """

# Тело шаблона
TABLE_CONTENT = """
                    <tr>
                      <td>{name}</td>
                      <td>{value}</td>
                    </tr>
                """

# Наполнение полей ключ:значение тела шаблона
PRODUCT_SPEC = {
    'notebook': {
        'Диагональ': 'diagonal',
        'Тип дисплея': 'display_type',
        'Частота процессора': 'processor_freq',
        'Оперативная память': 'ram',
        'Видеокарта': 'video',
        'Время работы аккумулятора': 'time_without_charge',
    },
    'smartphone': {
        'Диагональ': 'diagonal',
        'Тип дисплея': 'display_type',
        'Разрешение экрана': 'resolution',
        'Объем батереи': 'accum_volume',
        'Оперативная память': 'ram',
        'Наличие слота для SD карты': 'sd',
        'Максимальный объем SD карты': 'sd_volume_max',
        'Камера (МП)': 'main_cam_mp',
        'Фронтальная камера (МП)': 'front_cam_mp',
    }
}


# Получаем характеристики продукта из необходимой модели
def get_product_spec(product, model_name):
    table_content = ''
    for name, value in PRODUCT_SPEC[model_name].items():
        table_content += TABLE_CONTENT.format(name=name, value=getattr(product, value))
    return table_content


# Выводим собранный шаблон в html
@register.filter
def product_spec(product):
    model_name = product.__class__._meta.model_name
    if isinstance(product, Smartphone):
        if not product.sd:
            PRODUCT_SPEC['smartphone'].pop('Максимальный объем SD карты', None)
        else:
            PRODUCT_SPEC['smartphone']['Максимальный объем SD карты'] = 'sd_volume_max'

    # mark_safe сохраняет разметку и переводит текст в html шаблон
    return mark_safe(TABLE_HEAD + get_product_spec(product, model_name) + TABLE_TAIL)
